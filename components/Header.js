import React from "react";
import { View, StyleSheet, Text, Button } from "react-native";

import TitleText from "./TitleText";
import AppTheme from "../constants/AppTheme";

const Header = (props) => {
  return (
    <View style={styles.header}>
      <TitleText style={styles.headerText}>{props.title}</TitleText>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    width: "100%",
    height: 90,
    paddingTop: 36,
    backgroundColor: AppTheme.color.titleBarBackground,
    alignItems: "center",
    justifyContent: "center",
  },
  headerText: {
    color: AppTheme.color.titleBarForground,
  },
});

export default Header;
