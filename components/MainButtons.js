import React from "react";
import { View, StyleSheet } from "react-native";

import Button from "./Button";
import AppTheme from "../constants/AppTheme";

const MainButtons = (props) => {
  return (
    <View style={styles.buttonContainer}>
      <Button onPress={props.onPress} style={styles.buttonRefresh}>
        REFRESH
      </Button>
      <Button onPress={props.openSettings} style={styles.buttonSettings}>
        SETTINGS
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 20,
  },
  buttonRefresh: {
    padding: 15,
    marginBottom: 10,
    width: "50%",
    backgroundColor: AppTheme.color.primary,
    borderColor: AppTheme.color.primary,
    color: AppTheme.color.primaryText,
    fontFamily: AppTheme.font.bold,
    fontSize: 18,
  },
  buttonSettings: {
    padding: 15,
    width: "40%",
    backgroundColor: AppTheme.color.accent,
    borderColor: AppTheme.color.accent,
    color: AppTheme.color.primaryText,
    fontFamily: AppTheme.font.bold,
    fontSize: 14,
  },
});

export default MainButtons;
