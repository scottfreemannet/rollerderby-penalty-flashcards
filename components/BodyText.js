import React from "react";
import { Text, StyleSheet } from "react-native";

import AppTheme from "../constants/AppTheme";

const BodyText = (props) => <Text style={{ ...styles.text, ...props.style }}>{props.children}</Text>;

const styles = StyleSheet.create({
  body: {
    fontFamily: AppTheme.font.base,
  },
});

export default BodyText;
