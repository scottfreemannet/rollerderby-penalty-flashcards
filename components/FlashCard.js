import React from "react";
import { View, Text, StyleSheet } from "react-native";

import AppTheme from "../constants/AppTheme";
import BodyText from "../components/BodyText";
import TitleText from "./TitleText";

const FlashCard = (props) => {
  const { color, number, penalty, code, cue, image } = props.flashcardData;

  const skaterNumber = (props) => {
    return {
      backgroundColor: props.flashcardData.color,
      color: props.flashcardData.text,
    };
  };

  const renderCue = (props) => {
    return props.settings.cue ? <BodyText style={styles.cue}>{cue}</BodyText> : <></>;
  };

  const renderImage = (props) => {
    return props.settings.image ? <BodyText style={styles.image}>Image: {image}</BodyText> : <></>;
  };

  const renderCode = (props) => {
    return props.settings.code ? <BodyText style={styles.code}>NSO Code: {code}</BodyText> : <></>;
  };

  const flashCardContent = () => {
    if (Object.keys(props.flashcardData).length > 1) {
      return (
        <>
          <View style={styles.skaterNumberContainer}>
            <View style={skaterNumber(props)}>
              <BodyText style={styles.skaterNumber}>
                <Text style={skaterNumber(props)}>{number}</Text>
              </BodyText>
            </View>
          </View>
          <View style={styles.metadata}>
            <BodyText style={styles.penalty}>{penalty}</BodyText>

            {renderCue(props)}

            {renderImage(props)}

            {renderCode(props)}
          </View>
        </>
      );
    } else {
      return <TitleText>Press "refresh" to start!</TitleText>;
    }
  };

  return <View style={styles.flashcardContainer}>{flashCardContent()}</View>;
};

const styles = StyleSheet.create({
  flashcardContainer: {
    alignItems: "center",
    marginTop: 20,
  },
  skaterNumberContainer: {
    width: "100%",
    borderWidth: 5,
    borderColor: "black",
    borderRadius: 10,
  },
  skaterNumber: {
    textAlign: "center",
    paddingVertical: 10,
    fontSize: 40,
  },
  metadata: {
    alignItems: "center",
  },
  penalty: {
    fontSize: 40,
    marginBottom: 20,
  },
  cue: {
    fontSize: 20,
    marginBottom: 10,
  },
  image: {
    marginBottom: 10,
  },
  code: {
    fontSize: 20,
  },
});

export default FlashCard;
