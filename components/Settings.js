import React from "react";
import { View, Modal, Text, TouchableOpacity, StyleSheet } from "react-native";

import AppTheme from "../constants/AppTheme";
import BodyText from "../components/BodyText";
import TitleText from "./TitleText";
import Button from "../components/Button";

const Settings = (props) => {
  return (
    <>
      <View style={styles.modal}>
        <Modal animationType="fade" transparent={true} visible={props.visible}>
          <View style={styles.modalBody}>
            <View style={styles.settings}>
              <TitleText style={styles.heading}>Settings</TitleText>
              {/* <BodyText>Autorefresh every [x seconds]</BodyText>*/}
              <View style={styles.buttonGroup}>
                <Button style={styles.button} onPress={props.save.bind(this, { cue: !props.settings.monochrome })}>
                  Toggle monochrome
                </Button>
                <Button style={styles.button} onPress={props.save.bind(this, { cue: !props.settings.cue })}>
                  Toggle verbal cue
                </Button>
                <Button style={styles.button} onPress={props.save.bind(this, { image: !props.settings.image })}>
                  Toggle image
                </Button>
                <Button style={styles.button} onPress={props.save.bind(this, { code: !props.settings.code })}>
                  Toggle NSO code
                </Button>
              </View>
            </View>
            <Button style={styles.hideModalButton} onPress={props.setVisible}>
              Close
            </Button>
          </View>
        </Modal>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
  },
  modalBody: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  hideModalButton: {
    marginTop: 20,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "black",
    padding: 10,
    backgroundColor: "#eee",
  },
  settings: {
    alignItems: "flex-start",
  },
  heading: {
    marginBottom: 10,
  },
  buttonGroup: {
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    marginBottom: 10,
    width: "100%",
  },
});

export default Settings;
