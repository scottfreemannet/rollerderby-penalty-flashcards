import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";

import BodyText from "../components/BodyText";
import AppTheme from "../constants/AppTheme";

const Button = (props) => {
  return (
    <TouchableOpacity style={styles.buttonContainer} activeOpacity={0.8} onPress={props.onPress}>
      <BodyText style={{ ...styles.button, ...props.style }}>{props.children}</BodyText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    textAlign: "center",
    borderRadius: 10,
    borderWidth: 1,
  },
});

export default Button;
