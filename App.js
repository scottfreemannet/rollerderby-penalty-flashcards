import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import * as Font from "expo-font";
import { AppLoading } from "expo";

import getFlashCard from "./scripts/getFlashCard";

import AppTheme from "./constants/AppTheme";
import Header from "./components/Header";
import FlashCard from "./components/FlashCard";
import MainButtons from "./components/MainButtons";
import Settings from "./components/Settings";

const fetchFonts = () => {
  return Font.loadAsync({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });
};

export default function App() {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [settingsVisible, setSettingsVisible] = useState(false);
  const [flashcardData, setFlashcardData] = useState({});
  const [settings, setSettings] = useState({
    autorefresh: false,
    monochrome: false,
    image: false,
    cue: false,
    code: false,
  });

  if (!dataLoaded) {
    return (
      <AppLoading startAsync={fetchFonts} onFinish={() => setDataLoaded(true)} onError={(err) => console.log(err)} />
    );
  }

  const refreshFlashcard = () => {
    setFlashcardData(getFlashCard());
  };

  const saveSettings = (data) => {
    setSettings({ ...settings, ...data });
  };

  return (
    <View style={styles.screen}>
      <Header title="Penalty Flash Cards" />
      <View style={styles.canvas}>
        <FlashCard flashcardData={flashcardData} settings={settings} />
        <MainButtons onPress={refreshFlashcard} openSettings={setSettingsVisible.bind(this, !settingsVisible)} />
        <Settings
          settings={settings}
          save={(data) => saveSettings(data)}
          visible={settingsVisible}
          setVisible={() => setSettingsVisible(!settingsVisible)}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: AppTheme.color.backgroundColor,
  },
  canvas: {
    paddingHorizontal: 10,
  },
});
