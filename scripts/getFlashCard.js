import Penalties from "../constants/Penalties";
import NumberWords from "../constants/NumberWords";
import TeamColors from "../constants/TeamColors";

const randomize = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const getNumber = () => {
  let number = randomize(0, 9999);
  let trimmer = randomize(0, 3);
  if (trimmer === 1) number = number.toString().substring(0, 1);
  if (trimmer === 2) number = number.toString().substring(0, 2);
  if (trimmer === 3) number = number.toString().substring(0, 3);
  return number;
};

const getPenalty = () => {
  let index,
    rosterNumber = getNumber(),
    penaltyNumber = randomize(0, Penalties.length - 1),
    numbers = rosterNumber.toString().split(""),
    colorNumber = randomize(0, TeamColors.length - 1),
    words = `${TeamColors[colorNumber].background},`;

  for (index = 0; index < numbers.length; ++index) {
    words += ` ${NumberWords[numbers[index]]}`;
  }

  words += `, ${Penalties[penaltyNumber].name.toLowerCase()}`;

  return {
    penalty: Penalties[penaltyNumber].name,
    number: rosterNumber,
    color: TeamColors[colorNumber].background,
    text: TeamColors[colorNumber].foreground,
    image: Penalties[penaltyNumber].image,
    code: Penalties[penaltyNumber].nso,
    cue: words,
  };
};

const getFlashCard = () => {
  return getPenalty();
};

export default getFlashCard;
