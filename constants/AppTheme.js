export default {
  font: {
    base: "open-sans-bold",
    bold: "open-sans-bold",
  },
  color: {
    backgroundColor: "#ffffff",
    titleBarBackground: "#000",
    titleBarForground: "#fff",
    primary: "#00be66",
    accent: "#555",
    primaryText: "#ffffff",
  },
};
