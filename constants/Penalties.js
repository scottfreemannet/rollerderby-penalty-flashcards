export default [
  {
    name: "Back Block",
    image: "b.jpg",
    nso: "B",
  },
  {
    name: "High Block",
    image: "a.jpg",
    nso: "A",
  },
  {
    name: "Low Block",
    image: "l.jpg",
    nso: "L",
  },
  {
    name: "Head Block",
    image: "h.jpg",
    nso: "H",
  },
  {
    name: "Forearm",
    image: "f.jpg",
    nso: "F",
  },
  {
    name: "Leg Block",
    image: "e.jpg",
    nso: "E",
  },
  {
    name: "Illegal Contact",
    image: "c.jpg",
    nso: "C",
  },
  {
    name: "Illegal Assist",
    image: "c.jpg",
    nso: "C",
  },
  {
    name: "Early Hit",
    image: "c.jpg",
    nso: "C",
  },
  {
    name: "Late Hit",
    image: "c.jpg",
    nso: "C",
  },
  {
    name: "Out of Play Block",
    image: "c.jpg",
    nso: "C",
  },
  {
    name: "Direction",
    image: "d.jpg",
    nso: "D",
  },
  {
    name: "Stop Block",
    image: "d.jpg",
    nso: "D",
  },
  {
    name: "Multiplayer",
    image: "m.jpg",
    nso: "M",
  },
  {
    name: "Illegal Position",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Destruction",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Skating Out of Bounds",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Failure to Reform",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Failure to Return",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Failure to Yield",
    image: "p.jpg",
    nso: "P",
  },
  {
    name: "Cut",
    image: "x.jpg",
    nso: "X",
  },
  {
    name: "Illegal Re-Entry",
    image: "x.jpg",
    nso: "X",
  },
  {
    name: "Interference",
    image: "n.jpg",
    nso: "N",
  },
  {
    name: "Delay of Game",
    image: "n.jpg",
    nso: "N",
  },
  {
    name: "Illegal Procedure",
    image: "i.jpg",
    nso: "I",
  },
  {
    name: "Star Pass Violation",
    image: "i.jpg",
    nso: "I",
  },
  {
    name: "Pass Interference",
    image: "i.jpg",
    nso: "I",
  },
  {
    name: "Misconduct",
    image: "g.jpg",
    nso: "G",
  },
  {
    name: "Insubordination",
    image: "g.jpg",
    nso: "G",
  },
];
