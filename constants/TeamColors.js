export default [
  {
    foreground: "black",
    background: "white",
  },
  {
    foreground: "white",
    background: "black",
  },
  {
    foreground: "black",
    background: "red",
  },
  {
    foreground: "black",
    background: "pink",
  },
  {
    foreground: "white",
    background: "purple",
  },
  {
    foreground: "white",
    background: "green",
  },
  {
    foreground: "black",
    background: "yellow",
  },
  {
    foreground: "black",
    background: "orange",
  },
  {
    foreground: "white",
    background: "blue",
  },
];
